package com.playground.mycookbook.local.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import java.io.Serializable


@Entity
class Recipe constructor() : Serializable {
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0

    @ColumnInfo(name = "name")
    var name: String? = null

    @ColumnInfo(name = "ingredient")
    var ingredient: String? = null

    @ColumnInfo(name = "direction")
    var direction: String? = null

    @ColumnInfo(name = "image")
    var image: String? = null

    @ColumnInfo(name = "type")
    var type: Int? = null

    @Ignore
    constructor(name: String, ingredient: String, direction: String, image: String, type: Int) : this() {
        this.name = name
        this.ingredient = ingredient
        this.direction = direction
        this.image = image
        this.type = type
    }

}