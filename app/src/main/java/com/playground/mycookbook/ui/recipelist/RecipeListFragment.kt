package com.playground.mycookbook.ui.recipelist

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.playground.mycookbook.R
import com.playground.mycookbook.local.DatabaseBuilder
import com.playground.mycookbook.local.DatabaseHelperImpl
import com.playground.mycookbook.local.entity.Recipe
import com.playground.mycookbook.ui.recipeDetail.RecipeDetailActivity
import com.playground.mycookbook.ui.recipelist.adapter.RecipeRecyclerAdapter
import com.playground.mycookbook.utils.RecipeTypeAnnotation
import com.playground.mycookbook.utils.StatusAnnotation
import kotlinx.android.synthetic.main.recipe_list_fragment.*

class RecipeListFragment : Fragment() {

    companion object {
        const val EXTRAS_RECIPE_CATEGORY = "EXTRAS_RECIPE_CATEGORY"

        fun newInstance(@RecipeTypeAnnotation.Companion.RecipeType recipeType: Int) = RecipeListFragment().apply {
            val fragment = RecipeListFragment()
            fragment.arguments = Bundle().apply {
                putInt(EXTRAS_RECIPE_CATEGORY, recipeType)
            }
            return fragment
        }
    }

    private lateinit var viewModel: RecipeListViewModel
    private var recipeCategory: Int = 0
    private lateinit var mRecyclerAdapter: RecipeRecyclerAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        viewModel = ViewModelProviders.of(this, RecipeListViewFactory(DatabaseHelperImpl(DatabaseBuilder.getInstance(context?.applicationContext!!))))
            .get(RecipeListViewModel::class.java)
        return inflater.inflate(R.layout.recipe_list_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        arguments?.getInt(EXTRAS_RECIPE_CATEGORY)
            ?.let {
                recipeCategory = it
            }
    }

    override fun onStart() {
        super.onStart()

        refreshData()
    }

    private fun initView() {
        mRecyclerAdapter = RecipeRecyclerAdapter(requireActivity(), mutableListOf())
        mRecyclerAdapter.setOnItemClickListener(object : RecipeRecyclerAdapter.ItemOnClickListener {
            override fun Onclick(recipe: Recipe) {
                RecipeDetailActivity.start(requireActivity(), recipe)
            }
        })
        recipeRecyclerView.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = mRecyclerAdapter
        }
        swipetorefresh.setOnRefreshListener {
            refreshData()
        }
    }

    private fun refreshData() {
        viewModel.fetchRecipes(recipeCategory)
            .observe(this, Observer {
                when (it.status) {
                    StatusAnnotation.SUCCESS -> {
                        it.data?.let { recipes -> renderList(recipes) }
                        swipetorefresh.isRefreshing = false
                        recipeRecyclerView.visibility = View.VISIBLE
                    }

                    StatusAnnotation.LOADING -> {
                        swipetorefresh.isRefreshing = true
                        recipeRecyclerView.visibility = View.GONE
                    }

                    StatusAnnotation.ERROR -> {

                    }
                }
            })
    }

    private fun renderList(recipes: MutableList<Recipe>) {
        mRecyclerAdapter.setList(recipes)
        mRecyclerAdapter.notifyDataSetChanged()
    }

}
