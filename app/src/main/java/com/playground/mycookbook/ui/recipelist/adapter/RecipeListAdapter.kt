package com.playground.mycookbook.ui.recipelist.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter


class RecipeListAdapter : FragmentStateAdapter {

    private var fragments: List<Fragment>

    constructor(fragmentActivity: FragmentActivity, fragments: MutableList<Fragment>) : super(fragmentActivity) {
        this.fragments = fragments
    }

    constructor(fragment: Fragment, fragments: MutableList<Fragment>) : super(fragment) {
        this.fragments = fragments
    }

    override fun getItemCount(): Int {
        return fragments.size
    }

    override fun createFragment(position: Int): Fragment {
        return fragments[position]
    }
}