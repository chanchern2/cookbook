package com.playground.mycookbook.ui.recipeDetail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.playground.mycookbook.R
import com.playground.mycookbook.local.entity.Recipe

class RecipeDetailActivity : AppCompatActivity() {
    companion object {
        const val EXTRAS_RECIPE_DETAIL_DATA = "EXTRAS_RECIPE_DETAIL_DATA"

        fun start(context: Context, recipe: Recipe) {
            val intent = Intent(context, RecipeDetailActivity::class.java)
            intent.putExtra(EXTRAS_RECIPE_DETAIL_DATA, recipe)
            context.startActivity(intent)
        }
    }

    private lateinit var recipeDetailFragment: Fragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.recipe_detail_activity)
        if (savedInstanceState == null) {
            recipeDetailFragment = RecipeDetailFragment.newInstance(intent.getSerializableExtra(EXTRAS_RECIPE_DETAIL_DATA))
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, recipeDetailFragment)
                .commitNow()
        }
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.hide()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        recipeDetailFragment.onCreateOptionsMenu(menu, menuInflater)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        recipeDetailFragment.onOptionsItemSelected(item)
        return true
    }

}