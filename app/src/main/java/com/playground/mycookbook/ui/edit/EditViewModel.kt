package com.playground.mycookbook.ui.edit

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.playground.mycookbook.local.DatabaseHelper
import com.playground.mycookbook.local.entity.Recipe
import com.playground.mycookbook.utils.StatusObserver
import kotlinx.coroutines.launch

class EditViewModel (private val dbHelper: DatabaseHelper) : ViewModel() {

    fun sendData(data: Recipe, isEdit: Boolean): LiveData<StatusObserver<Recipe>> {
        val recipe = MutableLiveData<StatusObserver<Recipe>>()

        viewModelScope.launch {
            recipe.postValue(StatusObserver.loading(null))
            try {
                if(!isEdit) {
                    dbHelper.addRecipe(data)
                } else {
                    dbHelper.updateRecipe(data)
                }
                recipe.postValue(StatusObserver.success(data))

            } catch (e: Exception) {
                recipe.postValue(StatusObserver.error("Something Went Wrong", null))
            }
        }

        return recipe
    }
}