package com.playground.mycookbook.utils

import android.app.Activity
import android.content.Context
import android.view.inputmethod.InputMethodManager


class utils {

    companion object {
        fun hideSoftKeyboard(activity: Activity) {
            if (activity.currentFocus == null) {
                return
            }
            val inputMethodManager: InputMethodManager = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(
                    activity.currentFocus!!.windowToken, 0
            )
        }
    }
}