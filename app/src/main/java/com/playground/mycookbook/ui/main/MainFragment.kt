package com.playground.mycookbook.ui.main

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.tabs.TabLayoutMediator
import com.playground.mycookbook.MainActivity
import com.playground.mycookbook.R
import com.playground.mycookbook.ui.edit.EditActivity
import com.playground.mycookbook.ui.recipelist.RecipeListFragment
import com.playground.mycookbook.ui.recipelist.adapter.RecipeListAdapter
import com.playground.mycookbook.utils.StatusAnnotation
import kotlinx.android.synthetic.main.main_fragment.*

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private lateinit var viewModel: MainViewModel
    private lateinit var recipeListAdapter: RecipeListAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_main, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_add -> {
                EditActivity.start(requireActivity(), null)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as MainActivity).supportActionBar?.show()

        if((activity as MainActivity).permissionUtil.checkPermissions()) {
            viewModel.getRecipeCategory(requireActivity())
                .observe(this, Observer {
                    when (it.status) {
                        StatusAnnotation.SUCCESS -> {
                            val listOfFragment = arrayListOf<Fragment>()
                            it.data?.forEachIndexed { index, element ->
                                listOfFragment.add(RecipeListFragment.newInstance(index))
                            }
                            recipeListAdapter = RecipeListAdapter(this, listOfFragment)
                            recipeViewPager.apply { adapter = recipeListAdapter }
                            TabLayoutMediator(tabLayout, recipeViewPager) { tab, position ->
                                tab.text = it.data?.get(position)
                            }.attach()
                        }

                        StatusAnnotation.LOADING -> {

                        }

                        StatusAnnotation.ERROR -> {

                        }
                    }
                })
        }
    }

}