package com.playground.mycookbook.ui.recipelist.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.playground.mycookbook.R
import com.playground.mycookbook.local.entity.Recipe
import com.playground.mycookbook.ui.recipelist.adapter.RecipeRecyclerAdapter.RecipeHolder
import kotlinx.android.synthetic.main.recipe_list_item.view.*

class RecipeRecyclerAdapter(private val contex: Context, private var recipies: MutableList<Recipe>) : RecyclerView.Adapter<RecipeHolder>() {

    interface ItemOnClickListener {
        fun Onclick(recipe: Recipe)
    }

    private lateinit var onItemClickListener: ItemOnClickListener

    inner class RecipeHolder(v: View) : RecyclerView.ViewHolder(v), View.OnClickListener {
        private var view: View = v
        private lateinit var recipe: Recipe
        private lateinit var mOnClickListener: ItemOnClickListener

        init {
            v.setOnClickListener(this)
        }

        override fun onClick(v: View) {
            mOnClickListener.Onclick(recipe)
        }

        fun setOnClickListener(itemOnClickListener: ItemOnClickListener) {
            mOnClickListener = itemOnClickListener
        }

        fun bind(recipe: Recipe) {
            this.recipe = recipe

            Glide.with(contex)
                .load(recipe.image)
                .into(view.foodImage)

            view.foodName.text = recipe.name
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecipeHolder {
        val inflatedView = LayoutInflater.from(parent.context)
            .inflate(R.layout.recipe_list_item, parent, false)
        return RecipeHolder(inflatedView)
    }

    override fun getItemCount(): Int {
        return recipies.size
    }

    override fun onBindViewHolder(holder: RecipeHolder, position: Int) {
        holder.setOnClickListener(onItemClickListener)
        holder.bind(recipies.get(position))
    }

    fun setOnItemClickListener(onClickListener: RecipeRecyclerAdapter.ItemOnClickListener) {
        onItemClickListener = onClickListener
    }

    fun setList(recipesList: MutableList<Recipe>) {
        recipies = recipesList
    }

}