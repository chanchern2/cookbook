package com.playground.mycookbook.ui.edit

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.playground.mycookbook.R
import com.playground.mycookbook.local.entity.Recipe

class EditActivity : AppCompatActivity() {
    companion object {
        const val EXTRAS_RECIPE_DATA = "EXTRAS_RECIPE_DATA"

        fun start(context: Context, recipe: Recipe?) {
            val intent = Intent(context, EditActivity::class.java)
            intent.putExtra(EXTRAS_RECIPE_DATA, recipe)
            context.startActivity(intent)
        }
    }

    private lateinit var editFragment : Fragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.edit_activity)
        if (savedInstanceState == null) {
            editFragment = EditFragment.newInstance(intent.getSerializableExtra(EXTRAS_RECIPE_DATA))
            supportFragmentManager.beginTransaction()
                    .replace(R.id.container, editFragment)
                    .commitNow()
        }
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.hide()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        editFragment.onCreateOptionsMenu(menu, menuInflater)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        editFragment.onOptionsItemSelected(item)
        return true
    }

}