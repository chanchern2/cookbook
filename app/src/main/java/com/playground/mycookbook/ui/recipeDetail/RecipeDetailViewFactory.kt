package com.playground.mycookbook.ui.recipeDetail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.playground.mycookbook.local.DatabaseHelper
import com.playground.mycookbook.ui.edit.EditViewModel

class RecipeDetailViewFactory(private val dbHelper: DatabaseHelper) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return RecipeDetailViewModel(dbHelper) as T
    }
}