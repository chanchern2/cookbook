package com.playground.mycookbook.ui.recipeDetail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.playground.mycookbook.local.DatabaseHelper
import com.playground.mycookbook.local.entity.Recipe
import com.playground.mycookbook.utils.StatusObserver
import kotlinx.coroutines.launch

class RecipeDetailViewModel(private val dbHelper: DatabaseHelper) : ViewModel() {

    fun deleteData(data: Recipe): LiveData<StatusObserver<Recipe>> {
        val recipe = MutableLiveData<StatusObserver<Recipe>>()

        viewModelScope.launch {
            recipe.postValue(StatusObserver.loading(null))
            try {
                dbHelper.deleteRecipe(data)
                recipe.postValue(StatusObserver.success(data))

            } catch (e: Exception) {
                recipe.postValue(StatusObserver.error("Something Went Wrong", null))
            }
        }

        return recipe
    }

    fun fetchLatestData(id: Int): LiveData<StatusObserver<Recipe>> {
        val liveData = MutableLiveData<StatusObserver<Recipe>>()

        viewModelScope.launch {
            liveData.postValue(StatusObserver.loading(null))
            try {
                liveData.postValue(StatusObserver.success(dbHelper.getRecipeById(id)))
            } catch (e:java.lang.Exception) {
                liveData.postValue(StatusObserver.error("Something When Wrong", null))
            }
        }

        return liveData
    }
}