package com.playground.mycookbook.utils

import com.playground.mycookbook.utils.StatusAnnotation.Companion.ERROR
import com.playground.mycookbook.utils.StatusAnnotation.Companion.LOADING
import com.playground.mycookbook.utils.StatusAnnotation.Companion.SUCCESS

data class StatusObserver<out T>(@StatusAnnotation.Companion.Status val status: Int, val data: T?, val message: String?) {
    companion object {

        fun <T> success(data: T?): StatusObserver<T> {
            return StatusObserver(SUCCESS, data, null)
        }

        fun <T> error(msg: String, data: T?): StatusObserver<T> {
            return StatusObserver(ERROR, data, msg)
        }

        fun <T> loading(data: T?): StatusObserver<T> {
            return StatusObserver(LOADING, data, null)
        }

    }
}
