package com.playground.mycookbook.utils

import androidx.annotation.IntDef

class StatusAnnotation {

    companion object {
        @IntDef(SUCCESS, LOADING, ERROR)
        @Target(AnnotationTarget.FIELD)
        @Retention(AnnotationRetention.SOURCE)
        annotation class Status

        const val SUCCESS = 0
        const val LOADING = 1
        const val ERROR = 2
    }

}