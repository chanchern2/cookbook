package com.playground.mycookbook.ui.recipelist

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.playground.mycookbook.local.DatabaseHelper

class RecipeListViewFactory(private val dbHelper: DatabaseHelper) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return RecipeListViewModel(dbHelper) as T
    }
}