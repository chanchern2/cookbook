package com.playground.mycookbook.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.playground.mycookbook.local.dao.RecipeDao
import com.playground.mycookbook.local.entity.Recipe

@Database(entities = [Recipe::class], version = 2, exportSchema = true)
abstract class AppDatabase : RoomDatabase() {

    abstract fun recipeDao(): RecipeDao

}