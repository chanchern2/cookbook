package com.playground.mycookbook.local.dao

import androidx.room.*
import com.playground.mycookbook.local.entity.Recipe
import com.playground.mycookbook.utils.RecipeTypeAnnotation


@Dao
interface RecipeDao {
    @Query("SELECT * FROM recipe")
    suspend fun getAll(): MutableList<Recipe>

    @Query("SELECT * FROM recipe WHERE type == :type")
    suspend fun getRecipes(@RecipeTypeAnnotation.Companion.RecipeType type: Int?): MutableList<Recipe>

    @Query("SELECT * FROM recipe WHERE id == :id")
    suspend fun getRecipeById(id: Int?): Recipe

    @Insert
    suspend fun insertAll(recipes: MutableList<Recipe>)

    @Insert
    suspend fun addRecipe(recipe: Recipe)

    @Update
    suspend fun updateRecipe(recipe: Recipe)

    @Delete
    suspend fun delete(recipe: Recipe)
}