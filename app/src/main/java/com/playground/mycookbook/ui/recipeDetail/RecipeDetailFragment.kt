package com.playground.mycookbook.ui.recipeDetail

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.playground.mycookbook.R
import com.playground.mycookbook.local.DatabaseBuilder
import com.playground.mycookbook.local.DatabaseHelperImpl
import com.playground.mycookbook.local.entity.Recipe
import com.playground.mycookbook.ui.edit.EditActivity
import com.playground.mycookbook.ui.recipeDetail.RecipeDetailActivity.Companion.EXTRAS_RECIPE_DETAIL_DATA
import com.playground.mycookbook.utils.StatusAnnotation
import kotlinx.android.synthetic.main.recipe_detail_fragment.*
import java.io.Serializable

class RecipeDetailFragment : Fragment(), View.OnClickListener {

    companion object {
        fun newInstance(serializable: Serializable?) = RecipeDetailFragment().apply {
            val fragment = RecipeDetailFragment()
            fragment.arguments = Bundle().apply {
                putSerializable(EXTRAS_RECIPE_DETAIL_DATA, serializable)
            }
            return fragment
        }
    }

    private lateinit var viewModel: RecipeDetailViewModel
    private var recipeData: Recipe = Recipe()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        viewModel = ViewModelProviders.of(this, RecipeDetailViewFactory(DatabaseHelperImpl(DatabaseBuilder.getInstance(context?.applicationContext!!))))
            .get(RecipeDetailViewModel::class.java)
        return inflater.inflate(R.layout.recipe_detail_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as RecipeDetailActivity).supportActionBar?.show()
        initView()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        arguments?.getSerializable(EXTRAS_RECIPE_DETAIL_DATA)
            ?.let { serializeData ->
                recipeData = (serializeData as Recipe)
            }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {

            android.R.id.home -> {
                (activity as RecipeDetailActivity).onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onResume() {
        super.onResume()
        initData()
    }

    fun initView() {
        detailDelete.setOnClickListener(this)
        detailEdit.setOnClickListener(this)
    }

    fun initData() {
        viewModel.fetchLatestData(recipeData.id)
            .observe(this, Observer {
                when (it.status) {
                    StatusAnnotation.SUCCESS -> {
                        progressingBar.visibility = GONE
                        it.data?.let { recipeData = it }
                        refreshDataContent()
                    }

                    StatusAnnotation.LOADING -> {
                        progressingBar.visibility = VISIBLE
                    }

                    StatusAnnotation.ERROR -> {
                        progressingBar.visibility = GONE
                    }
                }
            })
    }

    fun refreshDataContent() {
        activity?.let { Glide.with(it)
            .load(recipeData.image)
            .into(detailImage)}
        detailName.text = recipeData.name
        detailIngredient.text = recipeData.ingredient
        detailDirection.text = recipeData.direction
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.detailEdit -> {
                activity?.let { EditActivity.start(it, recipeData) }
            }

            R.id.detailDelete -> {
                deleteRecipe()
            }
        }
    }

    fun deleteRecipe() {
        viewModel.deleteData(recipeData)
            .observe(this, Observer {
                when (it.status) {
                    StatusAnnotation.SUCCESS -> {
                        progressingBar.visibility = GONE
                        (activity as RecipeDetailActivity).finish()
                    }

                    StatusAnnotation.LOADING -> {
                        progressingBar.visibility = VISIBLE
                    }

                    StatusAnnotation.ERROR -> {
                        progressingBar.visibility = GONE
                    }
                }
            })
    }
}