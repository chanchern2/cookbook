package com.playground.mycookbook.ui.main

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.playground.mycookbook.R
import com.playground.mycookbook.utils.StatusObserver
import kotlinx.coroutines.launch

class MainViewModel : ViewModel() {

    fun getRecipeCategory(context: Context): LiveData<StatusObserver<MutableList<String>>> {
        val recipeCategory = MutableLiveData<StatusObserver<MutableList<String>>>()

        viewModelScope.launch {
            recipeCategory.postValue(StatusObserver.loading(null))
            try {
                recipeCategory.postValue(StatusObserver.success(context.resources.getStringArray(R.array.recipetypes).toMutableList()))
            } catch (e: Exception) {
                recipeCategory.postValue(StatusObserver.error("Encounter some issue", null))
            }
        }

        return recipeCategory
    }

}