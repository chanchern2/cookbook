package com.playground.mycookbook.utils

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.playground.mycookbook.utils.PermissionUtil.Companion.REQUEST_PERMISSION

abstract class BaseActivity : AppCompatActivity() {
    lateinit var permissionUtil: PermissionUtil

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        permissionUtil = PermissionUtil(this)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>,
                                            grantResults: IntArray) {
        when (requestCode) {
            REQUEST_PERMISSION ->{
                val isPermissionsGranted = permissionUtil
                    .processPermissionsResult(requestCode,permissions,grantResults)

                if(isPermissionsGranted){
                    onStart()
                } else {
                    Toast.makeText(this, "Permissions denied.", Toast.LENGTH_LONG)
                        .show()
                }
                return
            }
        }
    }
}