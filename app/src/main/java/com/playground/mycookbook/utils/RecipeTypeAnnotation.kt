package com.playground.mycookbook.utils

import androidx.annotation.IntDef

class RecipeTypeAnnotation {

    companion object {
        @IntDef(TYPE_ALL, TYPE_DISH, TYPE_DESSERT, TYPE_DRINK)
        @Target(AnnotationTarget.FIELD, AnnotationTarget.VALUE_PARAMETER)
        @Retention(AnnotationRetention.SOURCE)
        annotation class RecipeType

        const val TYPE_ALL = 0
        const val TYPE_DISH = 1
        const val TYPE_DESSERT = 2
        const val TYPE_DRINK = 3
    }

}

