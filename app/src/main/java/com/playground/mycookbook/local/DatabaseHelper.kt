package com.playground.mycookbook.local

import com.playground.mycookbook.local.entity.Recipe
import com.playground.mycookbook.utils.RecipeTypeAnnotation

interface DatabaseHelper {

    suspend fun getRecipes(): MutableList<Recipe>

    suspend fun getRecipes(@RecipeTypeAnnotation.Companion.RecipeType type: Int?): MutableList<Recipe>

    suspend fun getRecipeById(id: Int?): Recipe

    suspend fun insertAll(recipes: MutableList<Recipe>)

    suspend fun addRecipe(recipe: Recipe)

    suspend fun updateRecipe(recipe: Recipe)

    suspend fun deleteRecipe(recipe: Recipe)
}