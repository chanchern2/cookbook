package com.playground.mycookbook.local

import com.playground.mycookbook.local.entity.Recipe

class DatabaseHelperImpl(private val appDatabase: AppDatabase) : DatabaseHelper {

    override suspend fun getRecipes(): MutableList<Recipe> = appDatabase.recipeDao().getAll()

    override suspend fun getRecipes(type: Int?): MutableList<Recipe> = appDatabase.recipeDao().getRecipes(type)

    override suspend fun getRecipeById(id: Int?): Recipe = appDatabase.recipeDao().getRecipeById(id)

    override suspend fun insertAll(recipes: MutableList<Recipe>) = appDatabase.recipeDao().insertAll(recipes)

    override suspend fun addRecipe(recipe: Recipe) = appDatabase.recipeDao().addRecipe(recipe)

    override suspend fun updateRecipe(recipe: Recipe) = appDatabase.recipeDao().updateRecipe(recipe)

    override suspend fun deleteRecipe(recipe: Recipe) = appDatabase.recipeDao().delete(recipe)

}