package com.playground.mycookbook.ui.edit

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.playground.mycookbook.R
import com.playground.mycookbook.local.DatabaseBuilder
import com.playground.mycookbook.local.DatabaseHelperImpl
import com.playground.mycookbook.local.entity.Recipe
import com.playground.mycookbook.ui.edit.EditActivity.Companion.EXTRAS_RECIPE_DATA
import com.playground.mycookbook.utils.StatusAnnotation
import com.playground.mycookbook.utils.utils.Companion.hideSoftKeyboard
import kotlinx.android.synthetic.main.edit_fragment.*
import java.io.Serializable

class EditFragment : Fragment(), View.OnClickListener, AdapterView.OnItemSelectedListener {

    companion object {
        val REQUEST_PICKER = 250

        fun newInstance(serializable: Serializable?) = EditFragment().apply {
            val fragment = EditFragment()
            fragment.arguments = Bundle().apply {
                putSerializable(EXTRAS_RECIPE_DATA, serializable)
            }
            return fragment
        }
    }

    private lateinit var viewModel: EditViewModel
    private var recipeData: Recipe = Recipe()
    private var allowToSubmit = true
    private var isEdit = true

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        viewModel = ViewModelProviders.of(this, EditViewFactory(DatabaseHelperImpl(DatabaseBuilder.getInstance(context?.applicationContext!!))))
            .get(EditViewModel::class.java)
        return inflater.inflate(R.layout.edit_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as EditActivity).supportActionBar?.show()
        initView()
        initData()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        arguments?.getSerializable(EXTRAS_RECIPE_DATA)
            ?.let { serializeData ->
                recipeData = (serializeData as Recipe)
            }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_edit, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_submit -> {
                if (recipeData.name.isNullOrBlank() || recipeData.direction.isNullOrBlank() || recipeData.image.isNullOrBlank() || recipeData.ingredient.isNullOrBlank()) {
                    Toast.makeText(requireActivity(), "Please fill up the entire field", Toast.LENGTH_LONG)
                        .show()
                    return true
                }
                viewModel.sendData(recipeData, isEdit)
                    .observe(this, Observer {
                        when (it.status) {
                            StatusAnnotation.SUCCESS -> {
                                it.data
                                progressingBar.visibility = GONE
                                activity?.finish()
                            }

                            StatusAnnotation.LOADING -> {
                                hideSoftKeyboard(FragmentActivity())
                                allowToSubmit = false
                                progressingBar.visibility = VISIBLE
                            }

                            StatusAnnotation.ERROR -> {
                                progressingBar.visibility = GONE
                            }
                        }
                    })

                true
            }

            android.R.id.home -> {
                (activity as EditActivity).onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun initView() {

        if (!checkIsIteamValid()) {
            isEdit = false
            (activity as EditActivity).supportActionBar?.title = getString(R.string.add_activity_title)
        } else {
            isEdit = true
            (activity as EditActivity).supportActionBar?.title = getString(R.string.edit_activity_title)
        }

        ArrayAdapter.createFromResource(
                requireActivity(), R.array.recipetypes, android.R.layout.simple_spinner_item
        )
            .also { adapter ->
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                editType.adapter = adapter
            }

        editType.onItemSelectedListener = this
        editImage.setOnClickListener(this)

        editName.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun afterTextChanged(editable: Editable) {
                recipeData.name = editName.text.toString()
            }
        })

        editIngredient.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun afterTextChanged(editable: Editable) {
                recipeData.ingredient = editIngredient.text.toString()
            }
        })

        editDirection.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun afterTextChanged(editable: Editable) {
                recipeData.direction = editDirection.text.toString()
            }
        })

    }

    private fun initData() {
        changeImageState(recipeData.image)
        if(!recipeData.name.isNullOrBlank()) {
            editName.setText(recipeData.name)
        }

        if(!recipeData.ingredient.isNullOrBlank()) {
            editIngredient.setText(recipeData.ingredient)
        }

        if(!recipeData.direction.isNullOrBlank()) {
            editDirection.setText(recipeData.direction)
        }

        recipeData.type?.let { editType.setSelection(it) }
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.editImage -> {
                openGalleryForImage()
            }
        }
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        recipeData.type = position
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_PICKER) {
            recipeData.image = data?.data?.toString()
            changeImageState(recipeData.image)
        }

    }

    private fun openGalleryForImage() {
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, REQUEST_PICKER)
    }

    private fun changeImageState(imagePath: String?) {
        if (imagePath == null) {
            hint_editImage.visibility = View.VISIBLE
            iv_editImage.setImageResource(android.R.color.transparent)
        } else {
            hint_editImage.visibility = GONE
            activity?.let {
                Glide.with(it)
                    .load(imagePath)
                    .into(iv_editImage)
            }
        }
    }

    private fun checkIsIteamValid() : Boolean {
        if(!recipeData.name.isNullOrBlank() || !recipeData.direction.isNullOrBlank() || !recipeData.image.isNullOrBlank() || !recipeData.ingredient.isNullOrBlank()) {
            return true
        }

        return false
    }
}