package com.playground.mycookbook

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.playground.mycookbook.ui.main.MainFragment
import com.playground.mycookbook.utils.BaseActivity

class MainActivity : BaseActivity() {

    private lateinit var mainFragment: MainFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        mainFragment = MainFragment.newInstance()
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.container, mainFragment)
                    .commitNow()
        }
        supportActionBar?.hide()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        mainFragment.onCreateOptionsMenu(menu, menuInflater)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        mainFragment.onOptionsItemSelected(item)
        return true
    }
}