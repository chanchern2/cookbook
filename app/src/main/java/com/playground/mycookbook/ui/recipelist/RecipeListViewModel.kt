package com.playground.mycookbook.ui.recipelist

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.playground.mycookbook.local.DatabaseHelper
import com.playground.mycookbook.local.entity.Recipe
import com.playground.mycookbook.utils.RecipeTypeAnnotation
import com.playground.mycookbook.utils.StatusObserver
import kotlinx.coroutines.launch

class RecipeListViewModel(private val dbHelper: DatabaseHelper) : ViewModel() {

    fun fetchRecipes(@RecipeTypeAnnotation.Companion.RecipeType type: Int?): LiveData<StatusObserver<MutableList<Recipe>>> {
        val recipeList = MutableLiveData<StatusObserver<MutableList<Recipe>>>()

        viewModelScope.launch {
            recipeList.postValue(StatusObserver.loading(null))
            try {
                val recipesData = dbHelper.getRecipes(type)
                if (recipesData.isEmpty()) {
                    recipeList.postValue(StatusObserver.success(mutableListOf<Recipe>()))

                } else {
                    recipeList.postValue(StatusObserver.success(recipesData))
                }
            } catch (e: Exception) {
                recipeList.postValue(StatusObserver.error("Something Went Wrong", null))
            }
        }

        return recipeList
    }
}